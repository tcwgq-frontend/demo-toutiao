import request from '@/utils/request.js'

export const getArticleList = function(pageNo, pageSize) {
  return request.post('/api/article/page', {
    pageNo,
    pageSize
  }
  )
}
