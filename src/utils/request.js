import axios from 'axios'

const request = axios.create({
  baseURL: 'http://192.168.245.128:8080'
})

export default request
