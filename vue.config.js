const { defineConfig } = require('@vue/cli-service')

// 导入node.js path模块
const path = require('path')
const lessPath = path.join(__dirname, './src/theme.less')

module.exports = defineConfig({
  transpileDependencies: true,
  // 配置为''，打包后的项目可在本地直接打开
  publicPath: '',
  css: {
    loaderOptions: {
      less: {
        // 若 less-loader 版本小于 6.0，请移除 lessOptions 这一级，直接配置选项。
        lessOptions: {
          modifyVars: {
            // 直接覆盖变量
            // 'nav-bar-background-color': 'red'
            // 或者可以通过 less 文件覆盖（文件路径为绝对路径）
            hack: `true; @import "${lessPath}";`
          }
        }
      }
    }
  }
})
